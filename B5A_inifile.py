# ==============================================================================
"""INIFILE : demo for 'read_ini/write_ini' functions from the 'ezCLI' toolbox"""
# ==============================================================================
__author__  = "Christophe Schlick modified by Philippe Blasi"
__version__ = "1.0"
__date__    = "2022-11-12"
__usage__   = """
Simply press <ENTER> at each pause""" 
# ==============================================================================
from ezCLI import *
# ------------------------------------------------------------------------------
# Sample use cases for 'read_ini'
# ------------------------------------------------------------------------------
# first read the file as a text file to show its raw content
txt = read_txt('test-ini.txt')
pause(f">>> read INI file as a TXT file :\n{txt}")

# return the data stored in file and apply 'convert' to all items
ini = read_ini('test-ini.txt')
pause(f">>> read INI file and convert all data type :\n{ini}")

# return the data stored in file but keep all items as strings
ini = read_ini('test-ini.txt', raw=True)
pause(f">>> read INI file as keep all data as strings :\n{ini}")

# ------------------------------------------------------------------------------
# Sample use cases for 'write_ini'
# ------------------------------------------------------------------------------
# sample data containing 5 properties spread in 3 sections  
items = {'':{'a':1,'b':2},'AAA':{'aa':1.2,'bb':''},'BBB':{'cc':'#'}}
pause(f">>> sample structured data : \n{items}")

# replace the whole file and return the new file content
ini = write_ini('test.txt', items)
pause(f">>> write structured data as an INI file : \n{ini}")

# insert new section at tail and return new file content
ini = write_ini('test.txt', '\n[CCC]\nzz = 0', -1)
pause(f">>> insert new section at tail : \n{ini}")

# replace property stored at line 4 and return new file content
ini = write_ini('test.txt', {'aa':2.5}, 4, 5)
pause(f">>> replace property stored at line 4 : \n{ini}")
# ==============================================================================
