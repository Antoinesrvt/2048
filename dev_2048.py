from ezTK import *
import random

game_grid = []
grid_len = 0
is_game_over = False
scoreboard = []
previous_score = 0

TILE_COLORS = {
    0: 'grey',
    2: 'light blue',
    4: 'blue',
    8: 'purple',
    16: 'pink',
    32: 'orange',
    64: 'red',
    128: 'yellow',
    256: 'green',
    512: 'brown',
    1024: 'cyan',
    2048: 'magenta'
}

def initialize_game_grid(win):
   global game_grid

   for i in range(grid_len):
      row = []
      for j in range(grid_len):
         label = Label(win.board, text="", bg='grey', font=('Arial', 20), width=2, height=2)
         label.pack(side="left", padx=2, pady=2)
         row.append(label)
      game_grid.append(row)

def get_tile_value(text):
   try:
      return int(text)
   except ValueError:
      return 0

def merge_tiles(cells):
   merged_cells = [0] * grid_len
   j = 0
   for i in range(grid_len):
      if cells[i] != 0:
         if merged_cells[j] == 0:
            merged_cells[j] = cells[i]
         elif merged_cells[j] == cells[i]:
            merged_cells[j] *= 2
            j += 1
         else:
            j += 1
            merged_cells[j] = cells[i]
   return merged_cells

def get_empty_cells():
   empty_cells = []
   for i in range(grid_len):
      for j in range(grid_len):
         if get_tile_value(game_grid[i][j]['text']) == 0:
            empty_cells.append((i, j))
   return empty_cells

def add_new_tile():
   empty_cells = get_empty_cells()
   if empty_cells:
      i, j = random.choice(empty_cells)
      value = random.choice([2, 4])
      game_grid[i][j].config(text=str(value), bg=TILE_COLORS[value])

def update_game_grid():
   global game_grid
   for i in range(grid_len):
      for j in range(grid_len):
         tile = game_grid[i][j]
         value = get_tile_value(tile['text'])
         if value == 0:
            tile.config(text='', bg='grey')
         else:
            tile.config(text=str(value), bg=TILE_COLORS[value])


# move_tiles function with a number of colmun and rows of the game grid stored in the global variable grid_len
def move_tiles(widget:object, code:str, mods:tuple) -> None:
   global is_game_over
   if(is_game_over):
      return
   if check_game_over():
      when_game_over()
      is_game_over = True
      return
   global game_grid
   if code == 'Up':
      for j in range(grid_len):
         column = [get_tile_value(game_grid[i][j]['text']) for i in range(grid_len)]
         merged_column = merge_tiles(column)
         for i in range(grid_len):
            game_grid[i][j].config(text=str(merged_column[i]), bg=TILE_COLORS[merged_column[i]])
   elif code == 'Down':
      for j in range(grid_len):
         column = [get_tile_value(game_grid[i][j]['text']) for i in range(grid_len-1, -1, -1)]
         merged_column = merge_tiles(column)
         for i in range(grid_len):
            game_grid[i][j].config(text=str(merged_column[grid_len-1-i]), bg=TILE_COLORS[merged_column[grid_len-1-i]])
   elif code == 'Left':
      for i in range(grid_len):
         row = [get_tile_value(game_grid[i][j]['text']) for j in range(grid_len)]
         merged_row = merge_tiles(row)
         for j in range(grid_len):
            game_grid[i][j].config(text=str(merged_row[j]), bg=TILE_COLORS[merged_row[j]])
   elif code == 'Right':
      for i in range(grid_len):
         row = [get_tile_value(game_grid[i][j]['text']) for j in range(grid_len-1, -1, -1)]
         merged_row = merge_tiles(row)
         for j in range(grid_len):
            game_grid[i][j].config(text=str(merged_row[grid_len-1-j]), bg=TILE_COLORS[merged_row[grid_len-1-j]])
   add_new_tile()
   update_game_grid()



def get_max_tile():
      max_tile = 0
      for i in range(grid_len):
         for j in range(grid_len):
               value = get_tile_value(game_grid[i][j]['text'])
               if value > max_tile:
                  max_tile = value
      return max_tile



def store_score():
      global game_grid
      global scoreboard
      global previous_score
      score = get_max_tile()
      previous_score = score

      if(len(scoreboard) < 10):
         scoreboard.append(score)
      else:
         for i in range(len(scoreboard)):
            if previous_score > scoreboard[i]:
               scoreboard[i] = previous_score
               break

      scoreboard.sort(reverse=True)
      scoreboard = scoreboard[:10]
      with open('scoreboard.txt', 'w') as file:
         for score in scoreboard:
               file.write(str(score) + '\n')
      return score


def get_scoreboard():
      global scoreboard
      with open('scoreboard.txt', 'r') as file:
         for line in file:
               scoreboard.append(int(line))
      scoreboard.sort(reverse=True)
      print(scoreboard)
      scoreboard = scoreboard[:10]


def show_scoreboard():
      win.scoreboard = Frame(win)
      Label(win.scoreboard, text='Scoreboard', bg='black', fg='white', font=('Arial', 20)).pack()
      for score in scoreboard:
         Label(win.scoreboard, text=str(score), bg='black', fg='white', font=('Arial', 12)).pack()


def when_game_over():
   if(is_game_over):
      return
   store_score()
   win.game_over = Frame(win)
   Label(win.game_over, text='Game Over!', bg='black', fg='white', font=('Arial', 20)).pack()
   Button(win.game_over, text='New game', bg='black', fg='white', font=('Arial', 12), command=relaunch_game)

def check_game_over():
   if get_empty_cells():
      return False
   for i in range(grid_len):
      for j in range(grid_len):
         value = get_tile_value(game_grid[i][j]['text'])
         if value == 0:
            return False
         if i < 3 and value == get_tile_value(game_grid[i+1][j]['text']):
            return False
         if j < 3 and value == get_tile_value(game_grid[i][j+1]['text']):
            return False
   return True


def relaunch_game():
   global win
   global is_game_over
   win.board.destroy() 
   game_grid.clear()
   is_game_over = False
   win.game_over.destroy()
   get_scoreboard()
   create_game()

def create_game():
   global win
   win.menu.destroy()
   win.board = Frame(win, fold=grid_len, width=200, height=200)
   win.grid = initialize_game_grid(win)
   add_new_tile()
   add_new_tile()

def set_grid_len(number):
   global grid_len
   print(number)
   grid_len = number
   return grid_len


def main():
   global win
   win = Win(title='2048', op=4, width=200, key=move_tiles, height=200, bg='black')

   
   win.menu = Frame(win, ip=4, width=200, height=200)
   Label(win.menu, text='Welcome on the 2048 game', bg='grey', fg=TILE_COLORS[random.choice([2, 4, 8, 16, 32, 64, 128])], font=('Arial', 16)).pack()
   Button(win.menu, text='Launch game', bg='black', fg='white', font=('Arial', 12), command=create_game).pack()
   grid_possibilities = Frame(win.menu, ip=4, fold=4, width=200, height=200)
   Button(grid_possibilities, text="3x3",fg='white',command=lambda: set_grid_len(3)).pack(side="left")
   Button(grid_possibilities, text="4x4",fg='white',command=lambda: set_grid_len(4)).pack(side="left")
   Button(grid_possibilities, text="5x5",fg='white',command=lambda: set_grid_len(5)).pack(side="left")
   Button(grid_possibilities, text="6x6",fg='white',command=lambda: set_grid_len(6)).pack(side="left")

   
   get_scoreboard()
   show_scoreboard()
   
   win.loop()



# ==============================================================================
if __name__ == '__main__':
   main()
# ==============================================================================

